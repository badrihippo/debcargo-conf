Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: async_zip
Upstream-Contact: Harry <hello@majored.pw>
Source: https://github.com/Majored/rs-async-zip

Files: *
Copyright: 2021-2024 Harry <hello@majored.pw>
           2023-2024 Cognite AS
License: MIT

Files: examples/cli_compress.rs
 src/base/mod.rs
 src/base/read/io/compressed.rs
 src/base/read/io/entry.rs
 src/base/read/io/hashed.rs
 src/base/read/io/locator.rs
 src/base/read/io/mod.rs
 src/base/read/io/owned.rs
 src/base/read/mem.rs
 src/base/read/mod.rs
 src/base/read/seek.rs
 src/base/read/stream.rs
 src/base/write/compressed_writer.rs
 src/base/write/entry_stream.rs
 src/base/write/entry_whole.rs
 src/base/write/io/mod.rs
 src/base/write/io/offset.rs
 src/base/write/mod.rs
 src/date.rs
 src/entry/builder.rs
 src/entry/mod.rs
 src/error.rs
 src/file/builder.rs
 src/file/mod.rs
 src/lib.rs
 src/spec/attribute.rs
 src/spec/compression.rs
 src/spec/consts.rs
 src/spec/header.rs
 src/spec/mod.rs
 src/spec/parse.rs
 src/spec/version.rs
 src/string.rs
 src/tests/combined/mod.rs
 src/tests/mod.rs
 src/tests/read/compression/mod.rs
 src/tests/read/locator/mod.rs
 src/tests/read/mod.rs
 src/tests/spec/date.rs
 src/tests/spec/mod.rs
 src/tests/write/mod.rs
 src/tests/write/offset/mod.rs
 src/tokio/mod.rs
 src/tokio/read/fs.rs
 src/tokio/read/mod.rs
 src/utils.rs
 tests/common/mod.rs
 tests/compress_test.rs
 tests/decompress_test.rs
Copyright: 2021-2024 Harry [Majored] <hello@majored.pw>
License: MIT

Files: src/base/read/io/combined_record.rs
       src/tests/read/zip64/mod.rs
Copyright: 2023-2024 Cognite AS
           2023-2024 Harry [Majored] <hello@majored.pw>
License: MIT

Files: src/spec/extra_field.rs
       src/tests/write/zip64/mod.rs
Copyright: 2023-2024 Cognite AS
License: MIT 

Files: examples/actix_multipart.rs
Copyright: 2021-2024 Harry [Majored] <hello@majored.pw>
           2022-2024 FL33TW00D
License: MIT

Files: SPECIFICATION.md
Copyright: 1989-2014, 2018-2020, PKWARE Inc.
License: public-domain

Files: debian/*
Copyright:
 2023-2024 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2023-2024 Matthias Geiger <werdahias@riseup.net>
License: MIT

License: public-domain
 This file has been placed in the public domain.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
